# bank-ocr

http://codingdojo.org/kata/BankOCR/#problem-description

Project created with Leiningen

`lein run sample_data.txt` will read sample_data.txt and print the parsed account numbers (The program will read multiple input files separated by spaces)

Output will be stored in processed_sample_data.txt


`lein test` to run tests
