(ns bank-ocr.format
  (:gen-class)
  (:require [clojure.string :as s]
            [bank-ocr.validate]))

(defn format-account-number [account-number]
  (s/join account-number))

(defn format-account-numbers [account-numbers]
  (map format-account-number account-numbers))

;; todo move the validation out of this function
(defn format-account-number-with-validation [account-number]
  (str (format-account-number account-number)
       (bank-ocr.validate/validate account-number)))

(defn format-account-numbers-with-validation [account-numbers]
  (map format-account-number-with-validation account-numbers))

(defn format-file [account-numbers]
  (s/join \newline
          (format-account-numbers-with-validation account-numbers)))
