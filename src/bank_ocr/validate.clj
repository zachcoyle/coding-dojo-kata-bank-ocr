(ns bank-ocr.validate
  (:gen-class))


(defn has-element? [coll element]
  (some #(= element %) coll))

(defn mod-11 [n]
  (mod n 11))

(defn transform-digit [index digit]
  (* (+ 1 index) digit))

(defn passes-checksum? [account-number]
  (->> account-number
       reverse
       (map-indexed transform-digit)
       (reduce *)
       mod-11
       zero?))

(defn illegible? [account-number]
  (has-element? account-number \?))

(defn is-valid? [account-number]
  (and (= 9 (count account-number))
       (if (illegible? account-number)
         false
         (passes-checksum? account-number))))

(defn validate [account-number]
  (cond (illegible? account-number) " ILL"
        (passes-checksum? account-number) ""
        :else " ERR"))
