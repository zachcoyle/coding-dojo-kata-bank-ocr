(ns bank-ocr.parse
  (:gen-class)
  (:require [clojure.string :as s]
            [bank-ocr.data]))


(defn parse-account-number [digit-string]
  (bank-ocr.data/digit-map digit-string \?))

(defn cat-digit [accum line]
  (let [digit-slice (partition 3 line)]
    (if (empty? accum)
      digit-slice
      (map
       s/join
       (map flatten
            (map vector accum digit-slice))))))

(defn extract-digits [raw-account-number]
  (->> raw-account-number
       drop-last
       (reduce cat-digit [])
       (map parse-account-number)))

(defn split-account-numbers [raw-data]
  (partition 4 (s/split-lines raw-data)))

(defn split-character-rows [raw-account-number]
  (map
   (partial partition 3 3)
   raw-account-number))

(defn parse-file [file]
  (->> file
       slurp
       split-account-numbers
       (map extract-digits)))
