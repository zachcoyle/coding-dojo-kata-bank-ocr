(ns bank-ocr.core
  (:gen-class)
  (:require [bank-ocr.parse]
            [bank-ocr.format]))


(defn process-file [file]
  (->>
   file
   bank-ocr.parse/parse-file
   bank-ocr.format/format-file))

(defn -main [& files]
  (doseq [file files]
    (spit
     (str "processed_" file)
     (process-file file))))
