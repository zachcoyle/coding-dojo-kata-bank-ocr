(ns bank-ocr.data
  (:gen-class))

(def zero
  (str " _ "
       "| |"
       "|_|"))

(def one
  (str "   "
       "  |"
       "  |"))

(def two
  (str " _ "
       " _|"
       "|_ "))

(def three
  (str " _ "
       " _|"
       " _|"))

(def four
  (str "   "
       "|_|"
       "  |"))

(def five
  (str " _ "
       "|_ "
       " _|"))

(def six
  (str " _ "
       "|_ "
       "|_|"))

(def seven
  (str " _ "
       "  |"
       "  |"))

(def eight
  (str " _ "
       "|_|"
       "|_|"))

(def nine
  (str " _ "
       "|_|"
       " _|"))

(def digit-map {zero  0
                one   1
                two   2
                three 3
                four  4
                five  5
                six   6
                seven 7
                eight 8
                nine  9})
