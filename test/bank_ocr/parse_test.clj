(ns bank-ocr.parse-test
  (:require [clojure.test :refer :all]
            [bank-ocr.parse :refer :all]))


(deftest test-parse-digit
  (testing "parses well-formed digits"
    (is (= (parse-account-number " _ | ||_|") 0))
    (is (= (parse-account-number "     |  |") 1))
    (is (= (parse-account-number " _  _||_ ") 2))
    (is (= (parse-account-number " _  _| _|") 3))
    (is (= (parse-account-number "   |_|  |") 4))
    (is (= (parse-account-number " _ |_  _|") 5))
    (is (= (parse-account-number " _ |_ |_|") 6))
    (is (= (parse-account-number " _   |  |") 7))
    (is (= (parse-account-number " _ |_||_|") 8))
    (is (= (parse-account-number " _ |_| _|") 9))
    (is (= (parse-account-number "abcdefghi") \?))
    (is (= (parse-account-number " __  |  |") \?))
    (is (= (parse-account-number " _ |_| _ ") \?))
    (is (= (parse-account-number "         ") \?))))


(deftest test-extract-digit
  (testing "extracts digits"
    (is (= (extract-digits [" _     _  _     _  _  _  _  _ ___"
                            "| |  | _| _||_||_ |_   ||_||_|___"
                            "|_|  ||_  _|  | _||_|  ||_| _|___"
                            "                              ___"])
           [0 1 2 3 4 5 6 7 8 9 \?]))))