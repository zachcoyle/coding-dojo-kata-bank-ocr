(ns bank-ocr.format-test
  (:require [clojure.test :refer :all]
            [bank-ocr.format :refer :all]))


(deftest test-format-account-numbers
  (testing "formats account numbers"
    (is (= (format-account-number [4 5 7 5 0 8 0 0 0])   "457508000"))
    (is (= (format-account-number [6 6 4 3 7 1 4 9 5])   "664371495"))
    (is (= (format-account-number [1 2 3 4 5 6 7 8 9])   "123456789"))
    (is (= (format-account-number [8 6 1 1 0 \? \? 3 6]) "86110??36"))
    (is (= (format-account-numbers
            [[4 5 7 5 0 8 0 0 0]
             [6 6 4 3 7 1 4 9 5]
             [1 2 3 4 5 6 7 8 9]
             [8 6 1 1 0 \? \? 3 6]])
           ["457508000"
            "664371495"
            "123456789"
            "86110??36"]))))



(deftest test-formatting-with-validations
  (testing "formats account numbers with proper validation"
    (is (= (format-account-number-with-validation [4 5 7 5 0 8 0 0 0])   "457508000"))
    (is (= (format-account-number-with-validation [6 6 4 3 7 1 4 9 5])   "664371495 ERR"))
    (is (= (format-account-number-with-validation [1 2 3 4 5 6 7 8 9])   "123456789 ERR"))
    (is (= (format-account-number-with-validation [8 6 1 1 0 \? \? 3 6]) "86110??36 ILL"))
    (is (= (format-account-numbers-with-validation
            [[4 5 7 5 0 8 0 0 0]
             [6 6 4 3 7 1 4 9 5]
             [1 2 3 4 5 6 7 8 9]
             [8 6 1 1 0 \? \? 3 6]])
           ["457508000"
            "664371495 ERR"
            "123456789 ERR"
            "86110??36 ILL"]))
    (is (= (format-file
            [[4 5 7 5 0 8 0 0 0]
             [6 6 4 3 7 1 4 9 5]
             [1 2 3 4 5 6 7 8 9]
             [8 6 1 1 0 \? \? 3 6]])
           "457508000\n664371495 ERR\n123456789 ERR\n86110??36 ILL"))))
