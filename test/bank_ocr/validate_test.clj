(ns bank-ocr.validate-test
  (:require [clojure.test :refer :all]
            [bank-ocr.validate :refer :all]))


(deftest test-is-valid
  (testing "validates account numbers"
    (is (= (is-valid? [4 5 7 5 0 8 0 0 0])   true))
    (is (= (is-valid? [4 5 7 5 0 8 0 0 0 0]) false))
    (is (= (is-valid? [6 6 4 3 7 1 4 9 5])   false))
    (is (= (is-valid? [1 2 3 4 5 6 7 8 9])   false))
    (is (= (is-valid? [8 6 1 1 0 \? \? 3 6]) false))))

(deftest test-validate
  (testing "chooses correct validation description")
  (is (= (validate [4 5 7 5 0 8 0 0 0])   ""))
  (is (= (validate [6 6 4 3 7 1 4 9 5])   " ERR"))
  (is (= (validate [1 2 3 4 5 6 7 8 9])   " ERR"))
  (is (= (validate [8 6 1 1 0 \? \? 3 6]) " ILL")))